"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by sunday on 25.06.16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var task_model_1 = require('./task.model');
var task_service_1 = require("./task.service");
var TaskDetail = (function () {
    function TaskDetail(current, router, taskService) {
        this.current = current;
        this.router = router;
        this.taskService = taskService;
        this.close = new core_1.EventEmitter();
        this.navigated = false;
    }
    TaskDetail.prototype.ngOnInit = function () {
        var _this = this;
        if (this.current.snapshot.params['id'] !== null) {
            this.sub = this.current.params.subscribe(function (params) {
                var id = +params['id'];
                _this.taskService.getTask(id).then(function (task) { return _this.task = task; });
            });
            this.navigated = true;
        }
        else {
            this.navigated = false;
            this.task = new task_model_1.Task();
        }
    };
    TaskDetail.prototype.gotoTasks = function () {
        {
            this.router.navigate(['/tasks']);
        }
    };
    TaskDetail.prototype.goBack = function (savedTask) {
        if (savedTask === void 0) { savedTask = null; }
        this.close.emit(savedTask);
        if (this.navigated) {
            window.history.back();
        }
    };
    TaskDetail.prototype.save = function () {
        var _this = this;
        this.taskService.save(this.task)
            .then(function (task) { _this.task = task; _this.gotoTasks(); })
            .catch(function (error) { return _this.error = error; });
    };
    __decorate([
        core_1.Input()
    ], TaskDetail.prototype, "task");
    __decorate([
        core_1.Output()
    ], TaskDetail.prototype, "close");
    TaskDetail = __decorate([
        core_1.Component({
            selector: 'task-detail',
            templateUrl: 'app/task/task-detail.component.html',
            styleUrls: ['app/task/task-detail.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [task_service_1.TaskService],
            inputs: ['task']
        })
    ], TaskDetail);
    return TaskDetail;
}());
exports.TaskDetail = TaskDetail;
//# sourceMappingURL=task-detail.component.js.map