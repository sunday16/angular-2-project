/**
 * Created by sunday on 25.06.16.
 */
import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {Task} from "./task.model";
import {TaskDetail} from "./task-detail.component";
import {TaskService} from "./task.service";
import {NgForm} from '@angular/common';


@Component({
    selector: 'tasks',
    templateUrl: 'app/task/tasks.component.html',
    styleUrls: ['app/task/tasks.component.css'],
    directives: [TaskDetail, ROUTER_DIRECTIVES],
    providers: [TaskService]

})

export class TasksComponent implements OnInit {
    title = 'List of Tasks';
    public tasksList: Task[];
    public selectedTask:Task ;
    error: any;
    addingTask = false;
    newTask = new Task();
    submitted = false;
    active = true;



    ngOnInit() {
        this.getTasks();
    }
    
    getTasks() {
        this.taskService.getTasks().then(tasks => this.tasksList = tasks)
            .catch(error => this.error = error);
    }

    constructor(private taskService:TaskService) {
    }

    delete(task: Task, event: any){
        event.stopPropagation();
        this.taskService.delete(task)
            .then(res => {
                this.tasksList = this.tasksList.filter(h => h !== task);
                if(this.selectedTask === task) {this.selectedTask = null;}
            })
            .catch(error => this.error = error);
    }

    public onSelect(task:Task) {
        this.selectedTask = task;
    }


    public getSelectedClass(task:Task) {
        return {'selected': task === this.selectedTask}
    }


    save() {
        this.addingTask = false;
        this.taskService.save(this.newTask)
            .then(task => {this.newTask = task; this.getTasks()})
            .catch(error => this.error = error);
        /*this.newTask.title = undefined;
        this.newTask.description = undefined;*/
    }

    prepareTask(title: string, description: string){
        let length = this.tasksList.length;
        this.newTask.id = this.tasksList[length-1].id + 1;
        this.newTask.accepted = false;
        this.newTask.title = title;
        this.newTask.description = description;

    }
    
    addTask(){
        this.addingTask = true;
        this.selectedTask = null;


    }




}