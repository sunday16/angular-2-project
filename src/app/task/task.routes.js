"use strict";
var task_detail_component_1 = require('./task-detail.component');
var tasks_component_1 = require('./tasks.component');
exports.TASK_ROUTERS = [
    { path: '', component: tasks_component_1.TasksComponent },
    { path: 'detail/:id', component: task_detail_component_1.TaskDetail }
];
//# sourceMappingURL=task.routes.js.map