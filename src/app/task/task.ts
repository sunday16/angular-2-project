/**
 * Created by sunday on 25.06.16.
 */
import {Component} from "@angular/core";
import {ROUTER_DIRECTIVES} from "@angular/router";

@Component({
    selector: 'task-component',
    template: `
<div class="container-fluid">

<router-outlet></router-outlet>
</div> `,
    directives: [ROUTER_DIRECTIVES]

})

export class TaskComponent{

}