"use strict";
exports.TASKS = [
    { id: 1, title: 'Create structure', description: 'Create project structure - models and main files', accepted: true },
    { id: 2, title: 'Make a list of tasks', description: 'Create table tasks list in html', accepted: false },
    { id: 3, title: 'Create task details', description: 'Create details view in html', accepted: false },
    { id: 4, title: 'Make services', description: 'Make CRUD services', accepted: false }
];
//# sourceMappingURL=mock-task.js.map