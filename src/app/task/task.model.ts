/**
 * Created by sunday on 25.06.16.
 */
export class Task {
    id: number;
    title: string;
    description: string;
    accepted : boolean;
    
}