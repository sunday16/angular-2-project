"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
///<reference path="../../../typings/globals/core-js/index.d.ts"/>
/**
 * Created by sunday on 27.06.16.
 */
var core_1 = require('@angular/core');
require('rxjs/add/operator/toPromise');
var http_1 = require('@angular/http');
var TaskService = (function () {
    function TaskService(http) {
        this.http = http;
        this.taskUrl = 'build/tasks';
    }
    TaskService.prototype.getTasks = function () {
        return this.http.get(this.taskUrl)
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    TaskService.prototype.getTask = function (id) {
        return this.getTasks().then(function (tasks) { return tasks.filter(function (task) { return task.id === id; })[0]; });
    };
    TaskService.prototype.put = function (task) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var url = this.taskUrl + "/" + task.id;
        return this.http
            .put(url, JSON.stringify(task), { headers: headers })
            .toPromise()
            .then(function () { return task; })
            .catch(this.handleError);
    };
    TaskService.prototype.delete = function (task) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var url = this.taskUrl + "/" + task.id;
        return this.http.delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    };
    TaskService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    TaskService.prototype.save = function (task) {
        if (task.id) {
            return this.put(task);
        }
    };
    TaskService = __decorate([
        core_1.Injectable()
    ], TaskService);
    return TaskService;
}());
exports.TaskService = TaskService;
//# sourceMappingURL=task.service.js.map