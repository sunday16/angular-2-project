/**
 * Created by sunday on 25.06.16.
 */
import { Route} from '@angular/router';

import { TaskDetail } from './task-detail.component';
import { TasksComponent} from './tasks.component';
import { RouteInfo } from '../shared/router.metadata';

export const TASK_ROUTERS: Route[] = [
{ path: '', component: TasksComponent},
{path: 'detail/:id', component: TaskDetail}
];