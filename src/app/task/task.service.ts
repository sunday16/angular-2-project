///<reference path="../../../typings/globals/core-js/index.d.ts"/>
/**
 * Created by sunday on 27.06.16.
 */
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Http, Headers} from '@angular/http';
import {Task} from "./task.model";

@Injectable()
export class TaskService {

    taskUrl = 'build/tasks';

    constructor(private http:Http) {
    }

    getTasks():Promise<Task[]> {
        return this.http.get(this.taskUrl)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getTask(id: number){
        return this.getTasks().then(tasks => tasks.filter(task=> task.id === id)[0]);
    }

    private put(task:Task) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let url = `${this.taskUrl}/${task.id}`;
        
        return this.http
            .put(url, JSON.stringify(task), {headers: headers})
            .toPromise()
            .then(()=> task)
            .catch(this.handleError);
    }

    delete(task:Task) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.taskUrl}/${task.id}`;

        return this.http.delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error:any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
    
    save(task: Task): Promise<Task>{
        if(task.id){
            return this.put(task);
        }
    }
}