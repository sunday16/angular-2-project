"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by sunday on 25.06.16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var task_model_1 = require("./task.model");
var task_detail_component_1 = require("./task-detail.component");
var task_service_1 = require("./task.service");
var TasksComponent = (function () {
    function TasksComponent(taskService) {
        this.taskService = taskService;
        this.title = 'List of Tasks';
        this.addingTask = false;
        this.newTask = new task_model_1.Task();
        this.submitted = false;
        this.active = true;
    }
    TasksComponent.prototype.ngOnInit = function () {
        this.getTasks();
    };
    TasksComponent.prototype.getTasks = function () {
        var _this = this;
        this.taskService.getTasks().then(function (tasks) { return _this.tasksList = tasks; })
            .catch(function (error) { return _this.error = error; });
    };
    TasksComponent.prototype.delete = function (task, event) {
        var _this = this;
        event.stopPropagation();
        this.taskService.delete(task)
            .then(function (res) {
            _this.tasksList = _this.tasksList.filter(function (h) { return h !== task; });
            if (_this.selectedTask === task) {
                _this.selectedTask = null;
            }
        })
            .catch(function (error) { return _this.error = error; });
    };
    TasksComponent.prototype.onSelect = function (task) {
        this.selectedTask = task;
    };
    TasksComponent.prototype.getSelectedClass = function (task) {
        return { 'selected': task === this.selectedTask };
    };
    TasksComponent.prototype.save = function () {
        var _this = this;
        this.addingTask = false;
        this.taskService.save(this.newTask)
            .then(function (task) { _this.newTask = task; _this.getTasks(); })
            .catch(function (error) { return _this.error = error; });
        /*this.newTask.title = undefined;
        this.newTask.description = undefined;*/
    };
    TasksComponent.prototype.prepareTask = function (title, description) {
        var length = this.tasksList.length;
        this.newTask.id = this.tasksList[length - 1].id + 1;
        this.newTask.accepted = false;
        this.newTask.title = title;
        this.newTask.description = description;
    };
    TasksComponent.prototype.addTask = function () {
        this.addingTask = true;
        this.selectedTask = null;
    };
    TasksComponent = __decorate([
        core_1.Component({
            selector: 'tasks',
            templateUrl: 'app/task/tasks.component.html',
            styleUrls: ['app/task/tasks.component.css'],
            directives: [task_detail_component_1.TaskDetail, router_1.ROUTER_DIRECTIVES],
            providers: [task_service_1.TaskService]
        })
    ], TasksComponent);
    return TasksComponent;
}());
exports.TasksComponent = TasksComponent;
//# sourceMappingURL=tasks.component.js.map