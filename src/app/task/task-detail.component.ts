/**
 * Created by sunday on 25.06.16.
 */
import {Component, OnInit, Input,Output, EventEmitter} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, ActivatedRoute} from "@angular/router";
import {Task} from './task.model';
import {TaskService} from "./task.service";

@Component({
    selector: 'task-detail',
    templateUrl: 'app/task/task-detail.component.html',
    styleUrls: ['app/task/task-detail.component.css'],
    directives: [ROUTER_DIRECTIVES],
    providers: [TaskService],
    inputs: ['task']
})

export class TaskDetail implements OnInit {
    @Input()public task:Task;
    @Output() close = new EventEmitter();
    private sub:any;
    error: any;
    navigated = false;

    constructor(private current:ActivatedRoute, public router:Router, private taskService: TaskService) {

    }

    ngOnInit() {
        if(this.current.snapshot.params['id'] !== null) {
            this.sub = this.current.params.subscribe(params => {
                let id = +params['id'];
                this.taskService.getTask(id).then(task => this.task = task);

            });
            this.navigated = true;
        } else {
            this.navigated = false;
            this.task = new Task();
        }
    }
    
    
    
    public gotoTasks() {{
        this.router.navigate(['/tasks']);
    }}
    
    goBack(savedTask: Task = null){
        this.close.emit(savedTask);
        if(this.navigated){
            window.history.back();
        }
    }

    save() {
        this.taskService.save(this.task)
            .then(task => { this.task = task; this.gotoTasks();})
            .catch(error => this.error = error);
    }

}