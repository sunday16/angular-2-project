"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var router_service_1 = require('./shared/router.service');
var app_1 = require('./app');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var angular2_in_memory_web_api_1 = require('angular2-in-memory-web-api');
var in_memory_data_service_1 = require('./in-memory-data.service');
var forms_1 = require('@angular/forms');
platform_browser_dynamic_1.bootstrap(app_1.AppComponent, [router_service_1.APP_ROUTER_PROVIDERS, http_1.HTTP_PROVIDERS,
    { provide: http_2.XHRBackend, useClass: angular2_in_memory_web_api_1.InMemoryBackendService },
    { provide: angular2_in_memory_web_api_1.SEED_DATA, useClass: in_memory_data_service_1.InMemoryDataService },
    forms_1.disableDeprecatedForms(),
    forms_1.provideForms()
]);
//# sourceMappingURL=boot.js.map