import { provideRouter, RouterConfig } from '@angular/router';
import { Injectable } from '@angular/core';

import { HomeComponent } from '../home/home';
import { AboutUsComponent } from '../home/about/about';
import { ContactComponent } from '../home/contact/contact';
import { RouteInfo, MenuType } from './router.metadata';
import {TaskComponent} from "../task/task";
import {TASK_ROUTERS} from "../task/task.routes";

export const ROUTES: RouteInfo[] = [
  { path: '', component: HomeComponent, title: "Home", menuType: MenuType.BRAND },
  { path: 'tasks', component: TaskComponent, title: "Tasks", menuType: MenuType.LEFT, children: [ ...TASK_ROUTERS] },
  { path: 'about', component: AboutUsComponent, title: "About Us", menuType: MenuType.RIGHT },
  { path: 'contact', component: ContactComponent, title: "Contact", menuType: MenuType.RIGHT }
];

export const APP_ROUTES: RouterConfig = [
  ...ROUTES
];

@Injectable()
export class RouterService {
  constructor() {}

  public getRoutes() : RouteInfo[] {
    return ROUTES;
  }
}

export const APP_ROUTER_PROVIDERS = [
  provideRouter(APP_ROUTES)
];
