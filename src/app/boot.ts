import { bootstrap } from '@angular/platform-browser-dynamic';
import { APP_ROUTER_PROVIDERS } from './shared/router.service';
import { AppComponent } from './app';
import { HTTP_PROVIDERS } from '@angular/http';
import { XHRBackend } from '@angular/http';
import { InMemoryBackendService, SEED_DATA } from 'angular2-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { disableDeprecatedForms, provideForms } from '@angular/forms';

bootstrap( AppComponent, [ APP_ROUTER_PROVIDERS, HTTP_PROVIDERS,
    { provide: XHRBackend, useClass: InMemoryBackendService },
    { provide: SEED_DATA, useClass: InMemoryDataService},
    disableDeprecatedForms(),
    provideForms()
] );